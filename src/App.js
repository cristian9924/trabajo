import React from 'react';
import Usuario from'./Usuario.js';
import Globo from'./Globo.js';
import Escribir from'./Escribir.js';
import './assets/App.css';
import Caja from'./Caja.js';
import Img from'./Img.js';
import A from './img/A.jpg';
import B from './img/B.png';
import C from './img/C.png';

 export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      nombre:"",
      persona: [{
        id:1,
        nombre: 'ABC',
        mensaje:'ola',
      },
      {
        id:2,
        nombre: 'DFG',
        mensaje:'mmmmmm'
      },
      {id:3,
        nombre: 'HI',
        mensaje:'mmoo'
      }
      ]
    }
  }

componentDidMount(){}
  enviar(){

  }
  seleccionar(id){
    const contacto=this.state.persona.find((persona,index)=> persona.id === id)
    this.setState({nombre: contacto.nombre})
  }
  render(){
    const contacto=
    <div>
      {this.state.persona.map((persona, index) => {
        return (
        <div 
          className="Contacto" 
          onClick={()=> this.seleccionar(persona.id)}
          key= {index}>
        <Img />
          <span className="Nombre"> {persona.nombre}</span>
          <span className="preview">{persona.mensaje}</span>
        </div>
        )
      })
      }
    </div>

    return(
      <div style={{display:'flex', flexDirection:'row',
        width:'100vw', height:'100vh'}}>
        <div className="Lista">
          <Usuario/>
          {contacto}
        </div>
        <Caja
        nombre= {this.state.nombre}
        />

      </div>
      
   )
  }
}