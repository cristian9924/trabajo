import React from 'react';

 export default class Boton extends React.Component{
  constructor(props){
    super(props);
    this.state={  /*creamos un estado*/
    }
 
  }
  
  componentDidMount(){

   } 
  
  render(){

    const {titulo,onClick}= this.props; /*es lo que el padre manda*/
    return(
      <span
            style={{
             borderRadius:5,
             width:'auto', cursor:'pointer',
              margin:3, fontSize:42, textAlign:'center', padding:10
            }}
            onClick={()=>onClick()}
          > {titulo} </span>
    )
  }
 }