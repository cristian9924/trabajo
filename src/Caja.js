import React from 'react';
import Escribir from'./Escribir.js';
import Globo from'./Globo.js';

 export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
    }
  }

  componentDidMount(){}

   render(){
    return(
    <div style={{flex:4,display:'flex', flexDirection:'column'}}>
      <div className="Titulo">
      {this.props.nombre}
      </div>
      <div className="Caja">
        <Globo/>
      </div>
      <Escribir/>
    </div>
   )
  }
}
        